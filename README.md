# Locationbeauval.fr

Sample website to reference great location around the Zoo of Beauval, hosted with gitlab pages.

It's a static website with Vue & Vuetify, using the free theme: https://store.vuetifyjs.com/products/blog-theme-free

**Do you want to get listed ?**

Send an email at locationbeauval@gmail.com and I will review your offer.

## Local setup

```
# docker run -v $(pwd):/src/app -p 8081:8080 -it node bash
yarn install
yarn run serve
```
